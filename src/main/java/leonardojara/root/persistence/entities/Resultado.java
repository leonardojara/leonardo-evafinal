package leonardojara.root.persistence.entities;

public class Resultado {

    private String definitions;
    private String id;

    public String getDefinitions() {
        return definitions;
    }

    public void setDefinitions(String street) {
        this.definitions = definitions;
    }

    public String getId() {
        return id;
    }

    public void setId(String city) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getDefinitions() + ", " + getId();
    }
}
